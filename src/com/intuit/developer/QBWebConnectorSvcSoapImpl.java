/**
 * QBWebConnectorSvcSoapImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.intuit.developer;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import com.legalmatch.ws.quickbooks.db.DBUtils;
import com.legalmatch.ws.quickbooks.db.GeneralQueries;
import com.legalmatch.ws.quickbooks.pojo.CustomerRecord;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.ReplaceOptions;
import net.minidev.json.JSONArray;
import org.apache.axis.MessageContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.parsers.DOMParser;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;
import java.util.stream.Collectors;

public class QBWebConnectorSvcSoapImpl implements com.intuit.developer.QBWebConnectorSvcSoap {
	public static final String APPLIED_TO_TXN_RET = "AppliedToTxnRet";
	public static final String INVOICE_LINE_RET = "InvoiceLineRet";
	public static final String TXN_ID = "TxnID";
	private static Log log = LogFactory.getLog("qbWebConnector");

	private static Map<String, Integer> remainingCount = new HashMap<>();
	private static Map<String, String> iterators = new HashMap<>();
	private static Integer total = null;

	private final static String APP_VERSION = "1.1.19";
	private final static int BATCH_AMOUNT = 500;
	private final static String ACCOUNT = "Account";
	private final static String CUSTOMER = "Customer";
	private final static String INVOICE = "Invoice";
	private final static String RECEIVE_PAYMENT = "ReceivePayment";

	private final static List<String> types = Arrays.asList(CUSTOMER, INVOICE, RECEIVE_PAYMENT);
	private final static List<String> ignoreFields = Arrays.asList("Crcard", "Expdate", "SSNTIN");

    private final static String xmlPrefix = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><?qbxml version=\"6.0\"?><?qbxml version=\"6.0\"?><QBXML><QBXMLMsgsRq onError=\"continueOnError\">";
    private final static String queryRqStart = "<%sQueryRq iterator=\"Start\" requestID=\"%d\">%s</%sQueryRq>";
    private final static String queryRqStartParams = "<MaxReturned>%d</MaxReturned><ModifiedDateRangeFilter><FromModifiedDate>%s</FromModifiedDate><ToModifiedDate>%s</ToModifiedDate></ModifiedDateRangeFilter><IncludeLineItems>true</IncludeLineItems><OwnerID>0</OwnerID>";
	private final static String xmlPostfix = "</QBXMLMsgsRq></QBXML>";

	private final static String queryRqContinue = "<%sQueryRq iterator=\"Continue\" iteratorID=\"%s\" requestID=\"%d\">%s</%sQueryRq>";
	private final static String queryRqContinueParams = "<MaxReturned>%d</MaxReturned><IncludeLineItems>true</IncludeLineItems><OwnerID>0</OwnerID>";


	private final static String START = "START";
	private final static String CONTINUE = "CONTINUE";
	private static Map<String, String> customQueryRqParams = new HashMap<>();
	{
		customQueryRqParams.put(CUSTOMER + START, "<MaxReturned>%d</MaxReturned><OwnerID>0</OwnerID>");
		customQueryRqParams.put(CUSTOMER + CONTINUE, "<MaxReturned>%d</MaxReturned><OwnerID>0</OwnerID>");
	}

    public com.intuit.developer.AuthResponse authenticate2(java.lang.String strUserName, java.lang.String strPassword)
			throws java.rmi.RemoteException {
		return null;
	}

	public com.intuit.developer.ArrayOfString authenticate(java.lang.String strUserName, java.lang.String strPassword)
			throws java.rmi.RemoteException {
		@SuppressWarnings("unchecked")
		Hashtable<String,String> props = MessageContext.getCurrentContext().getService().getOptions();
		
		String serviceUserName = props.get("qbUserName");
		String servicePassword = props.get("qbPassword");
		String serviceFilePath = "E:\\Quickbooks\\legalmatch.QBW";//props.get("qbFile");
		String uuid = UUID.randomUUID().toString();

		if (serviceUserName.equals(strUserName) && servicePassword.equals(strPassword)) {
			return new ArrayOfString(new String[] { uuid, serviceFilePath });
		}

		return new ArrayOfString(new String[] { uuid, "nvu" });
	}

	public java.lang.String sendRequestXML(java.lang.String ticket, java.lang.String strHCPResponse,
			java.lang.String strCompanyFileName, java.lang.String qbXMLCountry, int qbXMLMajorVers, int qbXMLMinorVers)
			throws java.rmi.RemoteException {
		String xml = xmlPrefix;
		try {
			int iterator = 1;
			if(iterators.isEmpty()) {
                // Start query Request
			    for(String type : types) {
                    String query = queryRqStart;
			    	if(customQueryRqParams.containsKey(type + START)) {
						query = String.format(query, type, iterator++, customQueryRqParams.get(type + START), type);
					}else {
                    	query = String.format(query, type, iterator++, queryRqStartParams, type);
					}
			    	if(query.contains("FromModifiedDate")) {
			    		xml += String.format(query, BATCH_AMOUNT, getFromDate(type), getToDate());
					}else {
						xml += String.format(query, BATCH_AMOUNT);
					}
                }
			} else {
				// Continue Query Request
			    for(String type : types) {
				    if(iterators.get(type) != null) {
						String query = "";
						if(customQueryRqParams.containsKey(type + CONTINUE)) {
							query = String.format(queryRqContinue, type, iterators.get(type), iterator++, customQueryRqParams.get(type + CONTINUE), type);
						}else {
							query = String.format(queryRqContinue, type, iterators.get(type), iterator++, queryRqContinueParams, type);
						}
						xml += String.format(query, BATCH_AMOUNT);
                    }
                }
			}
            xml += xmlPostfix;
		} catch (Exception ex) {
			log.error("exception while constructing response xml", ex);
		}
		log.debug("sendRequestXML " + xml);
		return xml;
	}

	private String getFromDate(String table) {
		Connection connection = getConnection();
		String fromDate = "2019-10-01";
		try {
			ResultSet result = connection.prepareStatement("select DATE_FORMAT(MAX(TimeModified), '%Y-%m-%d') from qb_" + table.toLowerCase())
					.executeQuery();
			if(result.next()) {
				fromDate = result.getString(1);
			}
		} catch (SQLException e) {
			log.error("date query error", e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				log.error(e);
			}
		}
		return fromDate;
	}

	private String getToDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(calendar.getTime());
	}

	private void xmlAddIncludeRetElement(Document doc, String elementValue, Node queryNode) throws DOMException {
        Element iEle = doc.createElement("IncludeRetElement");
        Text iTxt = doc.createTextNode(elementValue);
        iEle.appendChild(iTxt);
        queryNode.appendChild(iEle);
    }

	public int receiveResponseXML(java.lang.String ticket, java.lang.String response, java.lang.String hresult,
			java.lang.String message) throws java.rmi.RemoteException {
		log.debug("receiveResponseXML start " + APP_VERSION);
		if(StringUtils.isBlank(response)) {
			log.debug("response xml is blank");
			return 100;
		}
		String xml = response;
		String jsonPrettyPrintString = null;

		log.debug("receiveResponseXML json parse");
		try {
			JSONObject xmlJSONObj = XML.toJSONObject(xml);
			jsonPrettyPrintString = xmlJSONObj.toString();
		} catch (JSONException je) {
			log.error(je);
		}
		if(StringUtils.isBlank(jsonPrettyPrintString)) {
			log.error("receiveResonseXML json is blank.");
			return 100;
		}
		jsonPrettyPrintString = jsonPrettyPrintString.replace("Desc", "Description");

		log.debug("receiveResponseeXML update records from json");
		for(String type : types) {
		    if(!updateMysql(jsonPrettyPrintString, type)) {
		        iterators.remove(type);
            }
        }
		log.debug("receiveResponseXML finishing");
		if(iterators.isEmpty()) {
			log.debug("Done.  All iterators removed.");
			total = null;
			return 100;
		}

		Integer remaining = remainingCount.values().stream().reduce(0, Integer::sum);
		int percent = 100 -((int) Math.round((remaining.doubleValue()*100.0)/total.doubleValue()));
		if(total != null && total.intValue() != 0 && (remaining.doubleValue() < total.doubleValue())) {
			log.debug(percent + "%");
			return percent;
		}
		return -1;

	}

	private boolean updateMysql(String jsonPrettyPrintString, String type) {
		return updateMysql(jsonPrettyPrintString, type, true);
	}
	private boolean updateMysql(String jsonPrettyPrintString, String type, boolean commit) {
		if(!jsonPrettyPrintString.contains(type+"Ret")) {
			log.debug("No " + type + "Ret");
			return false;
		}
		JSONArray objs = getJsonArray(jsonPrettyPrintString, type);

		if(objs.isEmpty()) {
			return false;
		}
		objs = ((JSONArray) objs.get(0));

		Connection connection = getConnection();
		if (connection == null) return false;
		log.debug(type + " " + objs.size());
		insertRecords(objs, type, connection, commit);
		log.debug("Insert Done " + type);
		return true;
	}

	private JSONArray getJsonArray(String jsonPrettyPrintString, String type) {
		ReadContext ctx = JsonPath.parse(jsonPrettyPrintString);
		log.debug("START****");
		for(String aType : types) {
		    setIterator(ctx, aType);
        }

		if(total == null) {
			total = remainingCount.values().stream().reduce(0, Integer::sum) + (3* BATCH_AMOUNT);
		}
		log.debug("END****");
		return ctx.read(String.format("$..%sRet",type));
	}

	private void setIterator(ReadContext ctx, String type) {
		String jsonSelector = String.format("$..%sQueryRs", type);

		if(((JSONArray) ctx.read(jsonSelector)).isEmpty()) {
			iterators.remove(type);
			remainingCount.remove(type);
		}else {
			iterators.put(type, String.valueOf(((JSONArray) ctx.read(jsonSelector + ".iteratorID")).get(0)));
			log.debug(type + " iteratorId = " + iterators.get(type));

			remainingCount.put(type ,Integer.valueOf(String.valueOf(((JSONArray) ctx.read(jsonSelector + ".iteratorRemainingCount")).get(0))));
			if(!remainingCount.containsKey(type) || remainingCount.get(type).equals(0)){
				iterators.remove(type);
				remainingCount.remove(type);
			}

		}
	}

	private Connection getConnection() {
		Connection connection = DBUtils.getJDBCConnection();
		if( null == connection ) {
			return null;
		}
		try {
			connection.setAutoCommit(false);
		} catch (SQLException ex) {
			log.error("driver doesn't support non-autocommit jdbc calls", ex);
			throw new RuntimeException(ex);
		}
		return connection;
	}

	private void insertRecords(JSONArray nodes, String type, Connection connection, boolean commit) {
		int inserted = 1;

		try {
			log.debug("start updates " + nodes.size());
			for (Object node : nodes) {
				PreparedStatement statement = null;
				LinkedHashMap<String,Object> map = ((LinkedHashMap)node);
				switch(type) {
					case CUSTOMER:
						map.put("_id", String.valueOf(map.get("FullName")) + String.valueOf(map.get("ListID")));
						statement = getCustomerPreparedStatement(connection, (LinkedHashMap)node);
						break;
					case ACCOUNT:
						map.put("_id", String.valueOf(map.get("ListID")));
						statement = getAccountPreparedStatement(connection, (LinkedHashMap)node);
						break;
					case INVOICE:
						String customerId = String.valueOf(((LinkedHashMap<String,Object>)map.get("CustomerRef")).get("ListID"));
						String refNumber = String.valueOf(map.get("RefNumber"));
						String txnId = String.valueOf(map.get(TXN_ID));
						if("0".equals(txnId)) {
							map.put("_id", String.format("%s_%s",customerId, refNumber));
						}else {
							map.put("_id", txnId);
						}
						log.debug("_id = " + map.get("_id"));
						log.debug( String.valueOf(((LinkedHashMap<String,Object>)map.get("CustomerRef")).get("FullName")));
						statement = getInvoicePreparedStatement(connection, (LinkedHashMap)node);
						break;
					case RECEIVE_PAYMENT:
						map.put("_id", String.valueOf(map.get(TXN_ID)));
						statement = getReceivePaymentPreparedStatement(connection, (LinkedHashMap)node);
						break;

				}
				if(commit) {
					statement.executeUpdate();
				}
				log.debug("inserted " + type + " " + inserted++ + " out of " + nodes.size());
			}
			log.debug("end updates");
			log.debug("commit start");
			if(commit) {
				connection.commit();
			}
			log.debug("commit end");
		} catch (Exception e) {
			log.error("Exception", e);
		} finally {
			try {
				connection.close();
				log.debug("connection closed");
			} catch (SQLException ex) {
				log.error("could not close jdbc connection", ex);
			}
		}

	}

	private static String typeJsonCustomer = "{\"_id\":\"string\",\"AccountNumber\":\"string\",\"AltContact\":\"string\",\"AltPhone\":\"string\",\"Balance\":\"double\",\"BillAddress\":\"document\",\"BillAddress_Addr1\":\"string\",\"BillAddress_Addr2\":\"string\",\"BillAddress_Addr3\":\"string\",\"BillAddress_Addr4\":\"string\",\"BillAddress_City\":\"string\",\"BillAddress_Country\":\"string\",\"BillAddress_Note\":\"string\",\"BillAddress_PostalCode\":\"string\",\"BillAddress_State\":\"string\",\"BillAddressBlock\":\"document\",\"BillAddressBlock_Addr1\":\"string\",\"BillAddressBlock_Addr2\":\"string\",\"BillAddressBlock_Addr3\":\"string\",\"BillAddressBlock_Addr4\":\"string\",\"BillAddressBlock_Addr5\":\"string\",\"CompanyName\":\"string\",\"Contact\":\"string\",\"CustomerTypeRef\":\"document\",\"CustomerTypeRef_FullName\":\"string\",\"CustomerTypeRef_ListID\":\"string\",\"EditSequence\":\"string\",\"Email\":\"string\",\"Fax\":\"string\",\"FirstName\":\"string\",\"FullName\":\"string\",\"IsActive\":\"int\",\"JobDescription\":\"string\",\"JobStatus\":\"string\",\"LastName\":\"string\",\"ListID\":\"string\",\"MiddleName\":\"string\",\"Name\":\"string\",\"Notes\":\"string\",\"Phone\":\"string\",\"PreferredPaymentMethodRef\":\"document\",\"PreferredPaymentMethodRef_FullName\":\"string\",\"PreferredPaymentMethodRef_ListID\":\"string\",\"SalesRepRef\":\"document\",\"SalesRepRef_FullName\":\"string\",\"SalesRepRef_ListID\":\"string\",\"SalesTaxCodeRef\":\"document\",\"SalesTaxCodeRef_FullName\":\"string\",\"SalesTaxCodeRef_ListID\":\"string\",\"Salutation\":\"string\",\"ShipAddress\":\"document\",\"ShipAddress_Addr1\":\"string\",\"ShipAddress_Addr2\":\"string\",\"ShipAddress_Addr3\":\"string\",\"ShipAddress_Addr4\":\"string\",\"ShipAddress_City\":\"string\",\"ShipAddress_Country\":\"string\",\"ShipAddress_Note\":\"string\",\"ShipAddress_PostalCode\":\"string\",\"ShipAddress_State\":\"string\",\"ShipAddressBlock\":\"document\",\"ShipAddressBlock_Addr1\":\"string\",\"ShipAddressBlock_Addr2\":\"string\",\"ShipAddressBlock_Addr3\":\"string\",\"ShipAddressBlock_Addr4\":\"string\",\"ShipAddressBlock_Addr5\":\"string\",\"Sublevel\":\"string\",\"TermsRef\":\"document\",\"TermsRef_FullName\":\"string\",\"TermsRef_ListID\":\"string\",\"TimeCreated\":\"datetime\",\"TimeModified\":\"datetime\",\"TotalBalance\":\"double\",\"SFDealsID\":\"string\",\"SFAccountID\":\"string\",\"LeadSource\":\"string\",\"Clerk\":\"string\"}";
	private static JSONObject typesCustomer = new JSONObject(typeJsonCustomer);
	private static String insertSqlCustomer =	" insert into qb_customer (%s) values (%s) on duplicate key update %s";
	private PreparedStatement getCustomerPreparedStatement(Connection connection, LinkedHashMap data) throws SQLException, ParseException {
		PreparedStatement statement = getPreparedStatement(connection, data, typesCustomer, insertSqlCustomer);
		return statement;
	}

	private static String typeJsonPayment = "{  \"_id\": \"string\",  \"TxnID\": \"string\",  \"TimeCreated\": \"datetime\",  \"TimeModified\": \"datetime\",  \"EditSequence\": \"int\",  \"TxnNumber\": \"int\",  \"CustomerRef\": \"document\",  \"CustomerRef_FullName\": \"string\",  \"CustomerRef_ListID\": \"string\",  \"ARAccountRef\": \"document\",  \"ARAccountRef_FullName\": \"string\",  \"ARAccountRef_ListID\": \"string\",  \"TxnDate\": \"date\",  \"RefNumber\": \"int\",  \"TotalAmount\": \"double\",  \"PaymentMethodRef\": \"document\",  \"PaymentMethodRef_ListID\": \"string\",  \"PaymentMethodRef_FullName\": \"string\",  \"Memo\": \"string\",  \"DepositToAccountRef\": \"document\",  \"DepositToAccountRef_ListID\": \"string\",  \"DepositToAccountRef_FullName\": \"string\",  \"UnusedPayment\": \"double\",  \"UnusedCredits\": \"double\",  \"AppliedToTxnRet\": \"document\"}";
	private static JSONObject typesPayment = new JSONObject(typeJsonPayment);
	private static String insertSqlPayment =	" insert into qb_receivepayment (%s) values (%s) on duplicate key update %s";
	private PreparedStatement getReceivePaymentPreparedStatement(Connection connection, LinkedHashMap data) throws SQLException, ParseException {
		PreparedStatement statement = getPreparedStatement(connection, data, typesPayment, insertSqlPayment);
		return statement;
	}

	private static String typeJsonInvoice = "{  \"_id\":\"string\",  \"AppliedAmount\":\"double\",  \"ARAccountRef\":\"document\",  \"ARAccountRef_FullName\":\"string\",  \"ARAccountRef_ListID\":\"string\",  \"BalanceRemaining\":\"int\",  \"BillAddress\":\"document\",  \"BillAddress_Addr1\":\"string\",  \"BillAddress_Addr2\":\"string\",  \"BillAddress_Addr3\":\"string\",  \"BillAddress_Addr4\":\"string\",  \"BillAddress_City\":\"string\",  \"BillAddress_Note\":\"string\",  \"BillAddress_PostalCode\":\"string\",  \"BillAddress_State\":\"string\",  \"BillAddress_Country\":\"string\", \"BillAddressBlock\":\"document\",  \"BillAddressBlock_Addr1\":\"string\",  \"BillAddressBlock_Addr2\":\"string\",  \"BillAddressBlock_Addr3\":\"string\",  \"BillAddressBlock_Addr4\":\"string\",  \"BillAddressBlock_Addr5\":\"string\",  \"ClassRef\":\"document\",  \"ClassRef_FullName\":\"string\",  \"ClassRef_ListID\":\"string\",  \"CustomerMsgRef\":\"document\",  \"CustomerMsgRef_FullName\":\"string\",  \"CustomerMsgRef_ListID\":\"string\",  \"CustomerRef\":\"document\",  \"CustomerRef_FullName\":\"string\",  \"CustomerRef_ListID\":\"string\",  \"CustomerSalesTaxCodeRef\":\"document\",  \"CustomerSalesTaxCodeRef_FullName\":\"string\",  \"CustomerSalesTaxCodeRef_ListID\":\"string\",  \"DueDate\":\"date\",  \"EditSequence\":\"int\",  \"IsFinanceCharge\":\"boolean\",  \"IsPaid\":\"boolean\",  \"IsPending\":\"boolean\",  \"IsToBeEmailed\":\"boolean\",  \"IsToBePrinted\":\"boolean\",  \"Memo\":\"string\",  \"PONumber\":\"int\",  \"RefNumber\":\"int\",  \"SalesRepRef\":\"document\",  \"SalesRepRef_FullName\":\"string\",  \"SalesRepRef_ListID\":\"string\",  \"SalesTaxPercentage\":\"int\",  \"SalesTaxTotal\":\"int\",  \"ShipAddress\":\"document\",  \"ShipAddress_Addr1\":\"string\",  \"ShipAddress_Addr2\":\"string\",  \"ShipAddress_Addr3\":\"string\",  \"ShipAddress_Addr4\":\"string\",  \"ShipAddress_City\":\"string\",  \"ShipAddress_Note\":\"string\",  \"ShipAddress_PostalCode\":\"string\",  \"ShipAddress_State\":\"string\",  \"ShipAddress_Country\":\"string\", \"ShipAddressBlock\":\"document\",  \"ShipAddressBlock_Addr1\":\"string\",  \"ShipAddressBlock_Addr2\":\"string\",  \"ShipAddressBlock_Addr3\":\"string\",  \"ShipAddressBlock_Addr4\":\"string\",  \"ShipAddressBlock_Addr5\":\"string\",  \"ShipDate\":\"string\",  \"Subtotal\":\"double\",  \"TemplateRef\":\"document\",  \"TemplateRef_FullName\":\"string\",  \"TemplateRef_ListID\":\"string\",  \"TimeCreated\":\"datetime\",  \"TimeModified\":\"datetime\",  \"TxnDate\":\"date\",  \"TxnID\":\"string\",  \"TxnNumber\":\"int\", \"TermsRef\":\"document\", \"TermsRef_ListID\":\"string\",\"TermsRef_FullName\":\"string\", \"Other\":\"string\", \"SFAccountID\":\"string\", \"SFDealsID\":\"string\", \"InvoiceLineRet\":\"document\"}";
	private static JSONObject typesInvoice = new JSONObject(typeJsonInvoice);
	private static String insertSqlInvoice =	" insert into qb_invoice (%s) values (%s) on duplicate key update %s";
	private PreparedStatement getInvoicePreparedStatement(Connection connection, LinkedHashMap data) throws SQLException, ParseException {
		PreparedStatement statement = getPreparedStatement(connection, data, typesInvoice, insertSqlInvoice);
		return statement;
	}


	private static final String typeJsonAccount = "{\"_id\":\"string\",\"AccountNumber\":\"string\",\"AccountType\":\"string\",\"Balance\":\"double\",\"BankNumber\":\"string\",\"CashFlowClassification\":\"string\",\"Description\":\"string\",\"EditSequence\":\"int\",\"FullName\":\"string\",\"IsActive\":\"boolean\",\"ListID\":\"string\",\"Name\":\"string\",\"ParentRef\":\"document\",\"ParentRef_FullName\":\"string\",\"ParentRef_ListID\":\"string\",\"SpecialAccountType\":\"string\",\"Sublevel\":\"int\",\"TaxLineInfoRet\":\"document\",\"TaxLineInfoRet_TaxLineID\":\"int\",\"TaxLineInfoRet_TaxLineName\":\"string\",\"TimeCreated\":\"datetime\",\"TimeModified\":\"datetime\",\"TotalBalance\":\"double\"}";
	private static final JSONObject typesAccount = new JSONObject(typeJsonAccount);
	private static final String insertSqlAccount =	" insert into qb_account (%s) values (%s) on duplicate key update %s";
	private PreparedStatement getAccountPreparedStatement(Connection connection, LinkedHashMap data) throws SQLException, ParseException {
		PreparedStatement statement = getPreparedStatement(connection, data, typesAccount, insertSqlAccount);
		return statement;
	}

	private PreparedStatement getPreparedStatement(Connection connection, LinkedHashMap<String, Object> data, JSONObject types, String insertSql) throws SQLException, ParseException {
		List<String> fieldsFound = new ArrayList<>();
		StringBuilder fields = new StringBuilder();
		StringBuilder qMarks =  new StringBuilder();
		StringBuilder duplicate =  new StringBuilder();
		Map<String,Object> newData = new HashMap<>();
		for (String key : data.keySet()) {
			if (types.has(String.valueOf(key))) {
				if (types.get(String.valueOf(key)).equals("document")) {
					processDocumentObject(types, data, fieldsFound, fields, qMarks, duplicate, newData, key, connection);
				} else {
					processFieldObject(fieldsFound, fields, qMarks, duplicate, key);
				}
			} else if("DataExtRet".equals(String.valueOf(key))) {
				if(data.get("DataExtRet") instanceof LinkedHashMap) {
					LinkedHashMap map = (LinkedHashMap) data.get("DataExtRet");
					addCustomFieldSql(types, fieldsFound, fields, qMarks, duplicate, newData, map);

				} else if(data.get("DataExtRet") instanceof JSONArray){
					JSONArray customFields = (JSONArray) data.get("DataExtRet");
					for(Object customField : customFields) {
						LinkedHashMap map = (LinkedHashMap) customField;
						addCustomFieldSql(types, fieldsFound, fields, qMarks, duplicate, newData, map);
					}
				}
			} else {
				if(!ignoreFields.contains(key)) {
					log.debug("Missing field : " + key);
					log.debug(data.get(key));
				}
			}
		}
		data.putAll(newData);
		fields.deleteCharAt(fields.length() - 1);
		qMarks.deleteCharAt(qMarks.length() - 1);
		duplicate.deleteCharAt(duplicate.length() - 1);

		PreparedStatement statement = connection.prepareStatement(String.format(insertSql, fields, qMarks, duplicate));
		int index = setValues(statement, fieldsFound, types, data, 1);
		setValues(statement, fieldsFound, types, data, index);
		return statement;
	}

	private void processFieldObject(List<String> fieldsFound, StringBuilder fields, StringBuilder qMarks, StringBuilder duplicate, Object key) {
		if(!fieldsFound.contains(String.valueOf(key))) {
			fields.append(key).append(",");
			duplicate.append(key).append("=?,");
			fieldsFound.add(String.valueOf(key));
			qMarks.append("?,");
		}else {
			log.debug("duplicate field found : " + key);
		}
	}

	private void processDocumentObject(JSONObject types, LinkedHashMap data, List<String> fieldsFound, StringBuilder fields, StringBuilder qMarks, StringBuilder duplicate, Map<String, Object> newData, String key, Connection connection) throws SQLException, ParseException {
		if(data.get(key) instanceof LinkedHashMap) {
			handleDocument(types, data, (LinkedHashMap<String, Object>) data.get(key), fieldsFound, fields, qMarks, duplicate, newData, key, connection);
		} else if(data.get(key) instanceof JSONArray && (APPLIED_TO_TXN_RET.equals(key) || INVOICE_LINE_RET.equals(key))) {
			JSONArray list = (JSONArray) data.get(key);
			for(Object item : list) {
				handleDocument(types, data, (LinkedHashMap<String, Object>) item, fieldsFound, fields, qMarks, duplicate, newData, key, connection);
			}
		}else if(data.get(key) != null) {
			log.debug(String.format("No handling of Object type %s for the field %s. ", data.get(key).getClass(), key));
			log.debug(data.get(key));
		}
	}

	private void handleDocument(JSONObject types, LinkedHashMap data, LinkedHashMap<String, Object> dataMap, List<String> fieldsFound, StringBuilder fields, StringBuilder qMarks, StringBuilder duplicate, Map<String, Object> newData, String key, Connection connection) throws SQLException, ParseException {
		if(APPLIED_TO_TXN_RET.equals(key)) {
			handleAppliedToTxn(connection, data, dataMap);
			return;
		}
		if(INVOICE_LINE_RET.equals(key)) {
			handleInvoiceLineItem(connection, data, dataMap);
			return;
		}

		for (String docKey : dataMap.keySet()) {
			String field = String.format("%s_%s",key,docKey);
			if(!types.has(field)) {
				log.debug("MISSING DOCUMENT FIELD : " + field);
			}else if(!fieldsFound.contains(field)) {
				newData.put(field, dataMap.get(docKey));
				fields.append(field).append(",");
				duplicate.append(field).append("=?,");
				fieldsFound.add(field);
				qMarks.append("?,");
			}else {
				log.debug("duplicate document field found : " + field);
			}
		}
	}

	private void handleInvoiceLineItem(Connection connection, LinkedHashMap data, LinkedHashMap<String, Object> dataMap) throws SQLException, ParseException {
		List<String> fieldsFound = new ArrayList<>();
		StringBuilder fields = new StringBuilder();
		StringBuilder qMarks =  new StringBuilder();
		StringBuilder duplicate =  new StringBuilder();

		String appliedToTxnRetTypes = "{\"TxnLineID\": \"string\",\"qb_invoice_id\": \"string\",\"Description\": \"string\"}";
		JSONObject types = new JSONObject(appliedToTxnRetTypes);
		dataMap.put("qb_invoice_id", data.get("_id"));

		String insertSql = "insert into qb_invoice_line(%s) values (%s) on duplicate key update %s";
		for (String key : dataMap.keySet()) {
			if (types.has(String.valueOf(key))) {
				processFieldObject(fieldsFound, fields, qMarks, duplicate, key);
			} else {
				log.debug("Not including field for qb_invoice_line : " + key);
			}
		}
		fields.deleteCharAt(fields.length() - 1);
		qMarks.deleteCharAt(qMarks.length() - 1);
		duplicate.deleteCharAt(duplicate.length() - 1);

		PreparedStatement statement = connection.prepareStatement(String.format(insertSql, fields, qMarks, duplicate));
		int index = setValues(statement, fieldsFound, types, dataMap, 1);
		setValues(statement, fieldsFound, types, dataMap, index);
		statement.executeUpdate();
	}

	private void handleAppliedToTxn(Connection connection, LinkedHashMap data, LinkedHashMap<String, Object> dataMap) throws SQLException, ParseException {
		List<String> fieldsFound = new ArrayList<>();
		StringBuilder fields = new StringBuilder();
		StringBuilder qMarks =  new StringBuilder();
		StringBuilder duplicate =  new StringBuilder();

		String appliedToTxnRetTypes = "{\"i_txnid\": \"string\",\"p_txnid\": \"string\",\"TxnType\": \"string\",  \"TxnDate\": \"date\",  \"RefNumber\": \"int\",  \"BalanceRemaining\": \"double\",  \"Amount\": \"double\"}";
		JSONObject types = new JSONObject(appliedToTxnRetTypes);
		dataMap.put("i_txnid", dataMap.get(TXN_ID));
		dataMap.put("p_txnid", data.get("_id"));

		String insertSql = "insert into qb_invoice_payment(%s) values (%s) on duplicate key update %s";
		for (String key : dataMap.keySet()) {
			if (types.has(String.valueOf(key))) {
				processFieldObject(fieldsFound, fields, qMarks, duplicate, key);
			} else if(!TXN_ID.equals(key)){
				log.debug("Missing field : AppliedToTxn : " + key);
			}
		}
		fields.deleteCharAt(fields.length() - 1);
		qMarks.deleteCharAt(qMarks.length() - 1);
		duplicate.deleteCharAt(duplicate.length() - 1);

		PreparedStatement statement = connection.prepareStatement(String.format(insertSql, fields, qMarks, duplicate));
		int index = setValues(statement, fieldsFound, types, dataMap, 1);
		setValues(statement, fieldsFound, types, dataMap, index);
		statement.executeUpdate();
	}

	private void addCustomFieldSql(JSONObject types, List<String> fieldsFound, StringBuilder fields, StringBuilder qMarks, StringBuilder duplicate, Map<String, Object> newData, LinkedHashMap map) {
		String fieldName = ((String) map.get("DataExtName")).replaceAll("\\s+", "")
				.replaceAll("[#\\./]", "");
		String value = String.valueOf(map.get("DataExtValue"));
		if (types.has(fieldName)) {
			newData.put(fieldName, value);
			fields.append("`" + fieldName + "`").append(",");
			duplicate.append("`" + fieldName + "`").append("=?,");
			fieldsFound.add(fieldName);
			qMarks.append("?,");
		}else {
			if(!ignoreFields.contains(fieldName)) {
				log.debug(String.format("MISSING CUSTOM FIELD : %s %s %s", fieldName, value, map.get("DataExtType")));
			}
		}
	}

	private int setValues(PreparedStatement statement, List<String> fieldsFound, JSONObject types, LinkedHashMap data, int index) throws SQLException, ParseException {
		for(String field : fieldsFound) {
			String type = String.valueOf(types.get(field));
			String value = data.get(field) == null ? null : String.valueOf(data.get(field));
			switch(type) {
				case "string":
					statement.setString(index++, value);
					break;
				case "boolean":
					statement.setBoolean(index++, Boolean.valueOf(value));
					break;
				case "int":
					statement.setLong(index++, NumberUtils.toLong(value));
					break;
				case "double":
					statement.setDouble(index++, NumberUtils.toDouble(value));
					break;
				case "datetime":
					java.util.Date date = DateUtils.parseDate(value,"yyyy-MM-dd'T'HH:mm:ssXXX");
					statement.setTimestamp(index++, new Timestamp(date.getTime()));
					break;
				case "date":
					java.util.Date date2 = DateUtils.parseDate(value,"yyyy-MM-dd");
					statement.setTimestamp(index++, new Timestamp(date2.getTime()));
					break;
			}
		}
		return index;
	}

	private void updateMongo(String jsonPrettyPrintString, String type) {
		try {
			MongoClient mongo = MongoClients.create("mongodb://10.5.1.14:27017");

			MongoDatabase db = mongo.getDatabase("legalmatch");
			MongoCollection collection = db.getCollection(type);

			// convert JSON to DBObject directly
			log.debug("Insert " + type);

			JSONArray objs = getJsonArray(jsonPrettyPrintString, type);
			if(objs.isEmpty()) {
				return;
			}
			objs = ((JSONArray) objs.get(0));
			log.debug(type + " " + objs.size());
			objs.forEach( node -> {
				Object id = ((LinkedHashMap)node).get("ListID");
				((LinkedHashMap)node).put("_id", String.valueOf(id));
				org.bson.Document docId = org.bson.Document.parse(String.format("{'_id':'%s'}",id));
				log.debug(docId);
				String json = new JSONObject((LinkedHashMap)node).toString();

				collection.replaceOne(docId, org.bson.Document.parse(json), new ReplaceOptions().upsert(true));
			});
			mongo.close();

		} catch (Exception e) {
			log.error(e);
		}
	}

	private List<CustomerRecord> filterMissingIds(List<CustomerRecord> records) {
		
		
		List<CustomerRecord> errors = records.stream()
			.filter(record -> record.getAccountSFNumber() == null)
			.collect(Collectors.toList());
			
		notifyFieldErrors(errors);
		
		records.removeAll(errors);
		
		return records;
	}
	
	
	public static void notifyFieldErrors(List<CustomerRecord> errors) {
		if(errors == null || errors.isEmpty()) {
			return;
		}
		
		String subject = "Quickbooks Customer(s) Missing Account Id ";
		String body = subject + "\n";
		body += errors.stream().map(r -> r.getFullName())
			.collect(Collectors.joining("\n"));
		
		log.info(body);
		sendEmail(subject, body);
	}
	
	public static void sendEmail(String subject, String body) {
		String to = "lmqb@legalmatch.com";
		String from = "lmqb@legalmatch.com";
		String host = "us-smtp-outbound-1.mimecast.com";//or IP address

		//Get the session object  
		Properties properties = System.getProperties();  
		properties.setProperty("mail.smtp.host", host);  
		Session session = Session.getDefaultInstance(properties);  

		//compose the message  
		try{  
			MimeMessage message = new MimeMessage(session);  
			message.setFrom(new InternetAddress(from));  
			message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));  
			message.setSubject(subject);  
			message.setText(body);  

			// Send message  
			Transport.send(message);
		}catch (Exception e) {
			log.error("Error Sending Error Debug Email", e);
		}  

	}

	public java.lang.String connectionError(java.lang.String ticket, java.lang.String hresult, java.lang.String message)
			throws java.rmi.RemoteException {
		return "done";
	}

	public java.lang.String getLastError(java.lang.String ticket) throws java.rmi.RemoteException {
		return "NoOp";
	}

	public java.lang.String closeConnection(java.lang.String ticket) throws java.rmi.RemoteException {
		return "OK";
	}
	
	
    /* format of response records being parsed:
     * <CustomerRet>
     *   <TimeCreated>2005-05-03T14:32:01-08:00</TimeCreated>
     *   <TimeModified>2010-03-15T08:41:25-08:00</TimeModified>
     *   <EditSequence>1268667685</EditSequence>
     *   <FullName>Doe, John A.</FullName>
     *   <TotalBalance>12780.00</TotalBalance>
     *   <AccountNumber>5555555555</AccountNumber>
     *    <Notes>As of Mar 2010... Tesing note etc...</Notes>
     * </CustomerRet>
     */
    private List<CustomerRecord> parseResponse( String xml ) {
    	/*
		 * </CustomerRet>
			</CustomerQueryRs>
			</QBXMLMsgsRs>
			</QBXML>
		 */
    	List<CustomerRecord> records = new ArrayList<CustomerRecord>();
        try {
        	log.debug("try parseResponse");
        	// Create a factory
        	DOMParser parser = new DOMParser();
        	parser.parse(new InputSource(new java.io.StringReader(xml)));
        	Document doc = parser.getDocument();
        	
            log.debug("loopsetup");
            // parse the document
            Element root = doc.getDocumentElement();
            NodeList xmlBlocks = root.getElementsByTagName("CustomerRet");
            int numBlocks = xmlBlocks.getLength();
            
            log.debug("loop numblocks = " + numBlocks);
            for( int i=0; i<numBlocks; i++ ) {
            	Element eElement = (Element)xmlBlocks.item(i);
            	String timeCreated = getElementValue(eElement,"TimeCreated");
            	String timeModified = getElementValue(eElement,"TimeModified");
            	String seq = getElementValue(eElement,"EditSequence");
            	String fullName = getElementValue(eElement,"FullName");
            	String balance = getElementValue(eElement,"TotalBalance");
            	String acct = getElementValue(eElement,"AccountNumber");
            	String note = getElementValue(eElement,"Notes");
            	log.debug(i + " ");
            	CustomerRecord record = new CustomerRecord();
            	record.setAcctCreateDateStamp(timeCreated);
            	record.setAcctModifyDateStamp(timeModified);
                record.setEditSequence(seq);
                record.setFullName(fullName);
                record.setTotalBalanceString(balance);
                record.setAccountSFNumber(acct);
                record.setNotes(note);
                records.add(record);                
            }
        } catch (Exception ex) {
            log.error("failed parsing customer records ", ex);
        }
        log.info("finished parsing customer records");
        return records;
    }
    
    private String getElementValue(Element eElement, String name) {
    	NodeList list = eElement.getElementsByTagName(name);
    	if(list.getLength() > 0) {
    		return list.item(0).getTextContent();
    	}
    	
    	return null; 
    }


    private int insertRecords( List<CustomerRecord> records ) {
        int inserted = 0;
        Connection connection = DBUtils.getJDBCConnection();
        if( null == connection ) {
            return inserted;
        }
        try {
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
        	log.error("driver doesn't support non-autocommit jdbc calls", ex);
            throw new RuntimeException(ex);
        }
        
        try {
            for( CustomerRecord record : records ) {
                record.dbInsert(connection, new Date());
                inserted++;
            }
            connection.commit();
        } catch (SQLException ex) {
        	log.error("batch update failed", ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
            	log.error("could not close jdbc connection", ex);
            }
        }
        log.info("inserted "+inserted+" records");
        return inserted;
    }

    private void cleanUpOldRecords( List<CustomerRecord> newRecords ) {
        Set<String> overDueAccountSFIds = new HashSet<String>();
        for( CustomerRecord record : newRecords ) {
            overDueAccountSFIds.add(record.getAccountSFNumber());
        }

        Set<String> oldSFIds = GeneralQueries.getCustomerRecordSFIds();
        oldSFIds.removeAll(overDueAccountSFIds);

        if( oldSFIds.isEmpty() ) {
        	log.info("no old records to remove");
            return;
        }

        log.info("removing the following old SF id accounts: "+oldSFIds);
        // these are all old records. Delete them.
        GeneralQueries.deleteRecords(oldSFIds);
    }
    
    // records that we received a ping.  This is used to monitor the service.
    private void logPing() {
	GeneralQueries.logServicePing();
    }

}
