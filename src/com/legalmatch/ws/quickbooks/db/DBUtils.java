/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.legalmatch.ws.quickbooks.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.axis.MessageContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author david.duman
 */
public class DBUtils {
	private static Log log = LogFactory.getLog("qbWebConnector");

	public static Connection getJDBCConnection() {
		@SuppressWarnings("unchecked")
		Hashtable<String, String> props = MessageContext.getCurrentContext().getService().getOptions();
		String driverString = props.get("jdbcDriverClass");
		String jdbcURL = props.get("jdbcURL");
		String jdbcUser = props.get("jdbcUser");
		String jdbcPassword = props.get("jdbcPassword");
		log.debug(" LOADING jdbc driver.... " + jdbcURL);
		
		try {
			
			InitialContext initCtx = new InitialContext();

			//Context envCtx = (Context) initCtx.lookup("java:comp/env");

			DataSource ds = (DataSource)initCtx.lookup("java:comp/env/jdbc/ReportsDB");

			
			
			Connection conn = ds.getConnection();//DriverManager.getConnection(jdbcURL, jdbcUser, jdbcPassword);
			conn.setAutoCommit(false);
			log.debug("JDBConnection finished");
			return conn;
		} catch (Exception ex) {
			log.error("unable to connect to the database.", ex);
			return null;
		}
	}

	public static void safeSetStatementValue(PreparedStatement statement, int index, Object insertValue)
			throws SQLException {
		if (insertValue instanceof java.util.Date) {
			java.util.Date date = (java.util.Date) insertValue;
			statement.setTimestamp(index, new Timestamp(date.getTime()));
		} else if (insertValue instanceof String) {
			statement.setString(index, (String) insertValue);
		} else {
			statement.setObject(index, insertValue);
		}
	}

}
