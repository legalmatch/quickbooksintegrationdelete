/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.legalmatch.ws.quickbooks.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author david.duman
 */
public class GeneralQueries {
    private static Log log = LogFactory.getLog("qbWebConnector");

    public static Set<String> getCustomerRecordSFIds() {
        Connection jdbcConnection = null;
        try {
            Set<String> ids = new HashSet<String>();
            String sql = " select " + "   qbr_sf_account " + " from" + "   qb_record " + " order by " + "   qbr_sf_account";
            jdbcConnection = DBUtils.getJDBCConnection();
            Statement statement = jdbcConnection.createStatement();
            ResultSet set = statement.executeQuery(sql);
            boolean success = set.first();
            while (success) {
                String id = set.getString(1);
                ids.add(id);
                success = set.next();
            }
            return ids;
        } catch (SQLException ex) {
        	log.error("failed to clear old records", ex);
            throw new RuntimeException(ex);
        } finally {
            try {
                if( null != jdbcConnection ) {
                    jdbcConnection.close();
                }
            } catch ( Throwable t ) {
            	log.error("failed to close connection", t);
            }
        }
    }

    public static int deleteRecords( Set<String> salesForceIds ) {
    	log.info("deleting old records. Total num salesforce IDs: "+salesForceIds.size());

        if( salesForceIds.isEmpty() ) {
            return 0;
        }

        int count = 0;
        List<String> ids = new ArrayList<String>(salesForceIds);
        for( int i=0; i<ids.size(); i++ ) {
            count += deleteRecord(ids.get(i));
        }
        log.info("deleted old records. Total deleted: "+count);
        return count;
    }

    public static int deleteRecord( String salesForceId ) {
        if( null == salesForceId || salesForceId.length() == 0 ) {
            return 0;
        }

        int count = 0;
        Connection jdbcConnection = null;
        try {
            StringBuilder sql = new StringBuilder(
                 " delete " +
                 " from " +
                 "   qb_record " +
                 " where " +
                 "   qbr_sf_account = ? ");

            jdbcConnection = DBUtils.getJDBCConnection();
            PreparedStatement statement = jdbcConnection.prepareStatement(sql.toString());
            statement.setString(1, salesForceId);
            count = statement.executeUpdate();
            log.debug("delete -- returned from execute update with count "+count+" for salesforce id "+salesForceId);
            statement.close();
            jdbcConnection.commit();
            log.info("delete successful: "+count+" old records deleted for salesforce id " + salesForceId);
        } catch (SQLException ex) {
            log.error("failed to delete old records for salesforce id " + salesForceId, ex);
        } finally {
            if( null != jdbcConnection ) {
                try {
                    jdbcConnection.close();
                } catch( Throwable t ) {
                	log.error("failed to close connection", t);
                }
            }
        }
        return count;
    }

    // records that we received a ping.  This is used to monitor the service.
    public static void logServicePing() {
        log.info("recording service ping received");
        Connection jdbcConnection = null;
        try {
            String sql = 
                 " insert into " +
                 "   qb_service_ping ( qsp_timestamp ) " +
                 " values " +
                 "   ( current_timestamp() ) ";
            jdbcConnection = DBUtils.getJDBCConnection();
            PreparedStatement statement = jdbcConnection.prepareStatement(sql.toString());
            statement.executeUpdate();
            log.debug("ping update -- inserted");
            statement.close();
            jdbcConnection.commit();
            log.debug("ping update -- transaction completed successfully");
        } catch (SQLException ex) {
        	log.error("ping update exception", ex);
        } finally {
            if( null != jdbcConnection ) {
                try {
                    jdbcConnection.close();
                } catch( Throwable t ) {
                	log.error("ping update failed to close connection", t);
                }
            }
        }
        log.info("ping update finished");
    }
}
