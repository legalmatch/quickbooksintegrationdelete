/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.legalmatch.ws.quickbooks.pojo;

import static com.legalmatch.ws.quickbooks.db.DBUtils.safeSetStatementValue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.legalmatch.ws.quickbooks.util.XMLUtils;

public class CustomerRecord {
	
	private static Log log = LogFactory.getLog("qbWebConnector");

    private String acctCreateDateStamp;
    private String acctModifyDateStamp;
    private String editSequence;
    private String fullName;
    private String totalBalanceString;
    private String accountSFNumber;
    private String notes;

    // for parsing timestamps
    //private static final DateFormat TIMESTAMP_FORMATTER = new ISO8601DateFormat();

    public CustomerRecord() {}

    public String getAccountSFNumber() {
        return accountSFNumber;
    }

    public void setAccountSFNumber(String accountSFNumber) {
        this.accountSFNumber = accountSFNumber;
    }

    // DATE in format: 2010-03-15T08:41:25-08:00
    public String getAcctCreateDateStamp() {
        return acctCreateDateStamp;
    }

    public Date getAcctCreateDate() {
        if( XMLUtils.isBlankString(this.acctCreateDateStamp) ) {
            return null;
        }
        return null;
    }


    // DATE in format: 2010-03-15T08:41:25-08:00
    public void setAcctCreateDateStamp(String acctCreateDateStamp) {
        this.acctCreateDateStamp = acctCreateDateStamp;
    }

    // DATE in format: 2010-03-15T08:41:25-08:00
    public String getAcctModifyDateStamp() {
        return acctModifyDateStamp;
    }

    public Date getAcctModifyDate() {
        if( XMLUtils.isBlankString(this.acctModifyDateStamp) ) {
            return null;
        }
        return null;
    }

    // DATE in format: 2010-03-15T08:41:25-08:00
    public void setAcctModifyDateStamp(String acctModifyDateStamp) {
        this.acctModifyDateStamp = acctModifyDateStamp;
    }

    public String getEditSequence() {
        return editSequence;
    }

    public void setEditSequence(String editSequence) {
        this.editSequence = editSequence;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTotalBalanceString() {
        return totalBalanceString;
    }

    public void setTotalBalanceString(String totalBalanceString) {
        this.totalBalanceString = totalBalanceString;
    }

    public void dbInsert ( Connection jdbcConnection, Date batchDate ) throws SQLException {
        AmountStatus amountStatus = getDuplicateStatus(jdbcConnection);
        
        RecordDBStatus duplicateStatus = amountStatus.status;
        if( duplicateStatus == RecordDBStatus.DUPLICATE ) {
            // ignore duplicates
            return;
        }

        // if not passed in, use current time
        if( null == batchDate ) {
            batchDate = new Date();
        }

        String nowLessSQL = amountStatus.balanceIsNowLess ?
            " qbr_pending_amount = ?, qbr_pending_date = ?, " : "";

        boolean isUpdate = duplicateStatus == RecordDBStatus.DIRTY ? true : false;        
        String updateSql =
            " update " +
            "   qb_record " +
            "  set " +
            "    qbr_edit_sequence = ?," +
            "    qbr_full_name = ?," +
            "    qbr_total_balance = ?," +
            "    qbr_notes = ?," +
            "    qbr_acct_create_date = ?," +
            "    qbr_acct_modify_date = ?," +
            nowLessSQL +
            "    qbr_batch_date = ?"+
            "  where " +
            "    qbr_sf_account = ?";

        String insertSql =
            " insert into " +
            "   qb_record " +
            " (" +
            "    qbr_sf_account," +
            "    qbr_edit_sequence," +
            "    qbr_full_name," +
            "    qbr_total_balance," +
            "    qbr_notes," +
            "    qbr_acct_create_date, " +
            "    qbr_acct_modify_date, " +
            "    qbr_batch_date"+
            " ) " +
            " values " +
            " ( ?, ?, ?, ?," +
            "   ?, ?, ?, ? " +
            " )";
        PreparedStatement statement = jdbcConnection.prepareStatement(isUpdate ? updateSql : insertSql);
        int index = 0;
        if( !isUpdate ) {
            safeSetStatementValue(statement, ++index, this.getAccountSFNumber());
        }
        safeSetStatementValue(statement, ++index, this.getEditSequence());
        safeSetStatementValue(statement, ++index, this.getFullName());
        safeSetStatementValue(statement, ++index, this.getTotalBalanceString());
        safeSetStatementValue(statement, ++index, this.getNotes());
        safeSetStatementValue(statement, ++index, this.getAcctCreateDate());
        safeSetStatementValue(statement, ++index, this.getAcctModifyDate());

        // partial payment patch
        if( isUpdate && amountStatus.balanceIsNowLess ) {
            safeSetStatementValue(statement, ++index, null);
            safeSetStatementValue(statement, ++index, null);

            // backdate batch date for this so the partial payment can be made
            Calendar backdate = Calendar.getInstance();
            backdate.add(Calendar.DAY_OF_MONTH, -5); // backdate it 5 days
            batchDate = backdate.getTime(); // set the reference so the
                             // back dated value will be applied for this
                             // partial payment
        }

        safeSetStatementValue(statement, ++index, batchDate);

        if( isUpdate ) {
            safeSetStatementValue(statement, ++index, this.getAccountSFNumber());
        }
        
        if( isUpdate ) {
            log.debug("updating for salesforce ID ["+this.accountSFNumber+"] and balance: ["+this.getTotalBalanceString()+"] ("+fullName+")");            
        } else {
            log.debug("inserting record for salesforce ID ["+this.accountSFNumber+"] and balance: ["+this.getTotalBalanceString()+"] ("+fullName+")");            
        }

        statement.executeUpdate();
    }

    private enum RecordDBStatus { NEW, DUPLICATE, DIRTY }

    private AmountStatus getDuplicateStatus( Connection jdbcConnection ) throws SQLException {
        String sql =
                " select " +
                "   qbr_edit_sequence," +
                "   qbr_total_balance " +
                " from " +
                "   qb_record " +
                " where " +
                "   qbr_sf_account = ?";
        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        safeSetStatementValue(statement, 1, this.getAccountSFNumber());
        ResultSet results = statement.executeQuery();
        boolean success = results.first();
        if( !success || results.wasNull() ) {
            return new AmountStatus(RecordDBStatus.NEW, this.getTotalBalanceString(), false);
        }

        String seq = results.getString(1);
        String balance = results.getString(2);
        boolean newBalanceIsLess = false;
        boolean newBalanceIsEqual = false;
        try {
            float dbBalance = Float.parseFloat(balance);
            float nwBalance = Float.parseFloat(this.getTotalBalanceString());
            if( nwBalance < dbBalance ) {
                newBalanceIsLess = true;
            }
            newBalanceIsEqual = nwBalance == dbBalance;

        } catch( NumberFormatException e ) {
            log.error("could not parse balance amount: " + balance, e);
        }

        // if sequence equals and the balance is not less than it was, it means the record hasn't changed since last updated.  Otherwise it has changed and is dirty (needs update)
        RecordDBStatus status = seq.equals(this.getEditSequence()) && newBalanceIsEqual ? RecordDBStatus.DUPLICATE : RecordDBStatus.DIRTY;

        // return status and current DB balance value
        return new AmountStatus(status, balance, newBalanceIsLess);
    }

    private static class AmountStatus {
        RecordDBStatus status;
        String amount;
        boolean balanceIsNowLess;

        AmountStatus( RecordDBStatus status,  String amount, boolean balanceIsNowLess  ) {
            this.status = status;
            this.amount = amount;
            this.balanceIsNowLess = balanceIsNowLess;
        }
    }   

    @Override
    public String toString() {
        return "CustomerRecord(usr: "+fullName+", seq: "+editSequence+")";
    }
}
