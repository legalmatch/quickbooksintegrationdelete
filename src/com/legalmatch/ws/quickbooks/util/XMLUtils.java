/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.legalmatch.ws.quickbooks.util;

import org.w3c.dom.*;
import org.w3c.dom.traversal.*;

public class XMLUtils {
    /**
     *  Given a DOM root element, this method will find the passed in child element and return
     *  the text between the element's opening and closing tags.  NOTE: This method assumes the
     *  xml element's tagname is unique in the document, so only one tag will be found.  If the
     *  named element is not found, or if the named element repeats, an empty string is returned.
     */
//    public static String extractSimple(Element rootElement, String propertyName) {
//            NodeList nodes = rootElement.getElementsByTagName(propertyName);
//            int length = nodes.getLength();
//            if( length == 1 ) {
//                    Node node = nodes.item(0);
//                    return getText(node);
//            }
//            return "";
//    }

    /**
     * workaround since I was unable to get QA1 to recognize the level3 dom xerces libraries we're using.
     * That is, L3 DOM has the method Node.getTextContent();  This method returns the text between an xml
     * element's opening and closing tags.
     */
//    public static String getText(Node node) {
//        if (node == null) return "";
//
//        // Set up the iterator
//        Document doc = node.getOwnerDocument();
//        DocumentTraversal traversable = (DocumentTraversal) doc;
//        int whatToShow = NodeFilter.SHOW_TEXT | NodeFilter.SHOW_CDATA_SECTION;
//        NodeIterator iterator = traversable.createNodeIterator(node, whatToShow, null, true);
//
//        // Extract the text
//        StringBuffer result = new StringBuffer();
//        Node current;
//        while ((current = iterator.nextNode()) != null) {
//            result.append(current.getNodeValue());
//        }
//        return result.toString();
//    }

    public static boolean isBlankString( String string ) {
        if( null == string || string.length() == 0 ) {
            return true;
        }

        string = string.replaceAll(" ", "");// trim out all spaces
        return string.isEmpty();
    }
}
